Names: Drew Brost and Max Lowenthal
Student IDs: 431940 and 435479
Link to the home page: http://ec2-54-186-115-66.us-west-2.compute.amazonaws.com/~stilkin/main_article_page.php

Description of Creative Portion
Part 1: A Random Page
Description:
We implemented a button on the home page that brings the user to a random post when they click on it.
The implementation is done using a randomization of the post_id values in the posts table that define each post and then select a single one

Part 2: Post Photos
Description:
We implemented a thumbnail style photo and photo upload capabilities via url link to each post.
The result of this is that if a post has a photo when its created, it will show up as a thumbnail next to the post, otherwise nothing appears.
When going to the main article page for each post, the photo (if there is one) will appear in a large size below the title and description of the post.

Part 3: User Profiles
Description: we implemented the capability to see all of the posts created by a specific user, aka a user profile.
By clicking on the username of the user who created a post, which can be found under every post on the main page, the user will be taken to a page that is populated by all the posts that user has made.

Part 4: Post Categories
Description:
We implemented the capability to organize your post into one of our pre-created categories (they're a bit silly).
This process begins when a user creates a post, and is offered, but not required, the option to put their post in one of our categories.
The result is that the category is diplayed on the homepage under the post, and by clicking on the category name, users are taken to a page that is populated by all the posts that match that category.
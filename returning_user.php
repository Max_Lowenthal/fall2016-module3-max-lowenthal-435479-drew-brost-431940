<!DOCTYPE html>

<html>
<head>
    <title>Adding User</title>
</head>

<body>

    <?php
	session_start();
	
	require 'database.php';
	
	//pulls in the username and password from the sign boxes
	$user = $_POST['username'];
	$password = $_POST['password'];
	
	echo "$user $password";

	//pulls the user_id and salted password for the username that was inputted into text box
	$check = $mysqli->prepare("select user_id, salt_pass from users where username =?");
	//this could be improved
	$check->bind_param('s', $user);
	$check->execute();
	
	$check->bind_result($id, $pass);
	$check ->fetch();
	$check -> close();
	$id = htmlspecialchars($id);
	
	//
	echo "This is the password at from the database: $pass";
	//encrypts the password put into the text box
	$temp = crypt($password, $pass);
	echo "<br>".$temp;
	//compares the newly encrpyted text box password with the previously salted password
	//if they match then the user and user_id is set to the signed in user and they are dropped off on the homepage signed in
	//if they dont match aka fail to login they are dropped off at the new user login page
	if ($temp == $pass) {
		$_SESSION['user'] = $user;
		$_SESSION['user_id'] = $id;
		header("location: main_article_page.php");
		} else {
			header("location: new_user_login.php");
		}
		
	
	?>
</body>
</html>
<!DOCTYPE html>
	<html>
      <?php
		//Starts session and determines if the user is logged in
		session_start();
		require 'database.php';
		$loggedIn = false;
		$loggedIn = (isset($_SESSION['user']));
		 
		//navBar code for navigating the site
			echo"<head>";
            echo"<title>Home Page</title>";
            echo"<link rel='stylesheet' href='http://www.w3schools.com/lib/w3.css'>";
			echo "</head>";
			echo"<body>";
            echo "<ul class='w3-navbar w3-purple'>";
                echo "<li><a href='main_article_page.php'>Home</a></li>";
                echo "<li><a href='post_article.php'>Post an Article!</a></li>";
				if ($loggedIn) { //potentail location for a page that links to a users profile or filters by signed in user
              echo"<li><a href='logout.php'>Logout</a></li>";
				   
				}
				else{
					echo"<li><a href='login_returning.php'>Sign in</a></li>";
				}
            echo"</ul>";
        
	
        //creates text at the top of the screen with current username or sigin in prompt
				$loggedIn = false;
				$loggedIn = (isset($_SESSION['user']));
                if ($loggedIn) {
                   $user = $_SESSION['user'];
										echo "<p> Welcome $user!</p>";
                }
                else{
										echo "<p>Welcome to our Site! Feel free to <a href='login_returning.php'>Sign In!</a> </p>";
                }
				
				$stmt = $mysqli->prepare("select users.user_id,post_id,title,description,link,username,photo,category from posts inner join users on users.user_id=posts.user_id order by posts.post_id");
				if(!$stmt){
					printf("Query Prep Failed %s \n",$mysqli->error);
				}
				
				//begins query to list posts
				$stmt->execute();
				$stmt->bind_result($user_id,$post_id,$title,$descrip,$link,$username, $photo,$category);
				
				while($stmt->fetch()){
					echo "<div class='w3-container w3-section'>";
					
					//checks if photo and prints it out if it's there
					if (!is_null($photo)) {
						$photo = htmlentities($photo);
						echo "<br><img src='$photo' alt='photo' style='width:152px;height:114px;'><br>";
					} else {
					}

					//button that links directly to the link
					$title = htmlentities($title);
					echo "<a class='w3-btn w3-hover-pink w3-round w3-small w3-white w3-border w3-border-black' href=$link>$title</a>";
					echo "<br>";
					$post_id1=htmlentities($post_id);
					//prints description and who wrote the article
					$descrip = htmlentities($descrip);
					$username=htmlentities($username);
					echo "\t <a href='article_page.php?varname=$post_id1'>$descrip </a><br>";
					echo "<a href='user_page.php?varname=$user_id'> By: $username</a>";

					//checks for category and prints out if its there
					if(!is_null($category) && $category!=''){
						$category = htmlentities($category);
						echo "<br><a href='category_page.php?varname=$category'> Category:$category</a><br>";
					}
					else{
					}
					//offers user the ability to edit and delete a post
					if($loggedIn && $user_id==$_SESSION['user_id']){
					echo "<form action='post_edit.php' method='POST'>";
					echo "<input type='hidden' name='title_content' value='$title'>";
					echo "<input type='hidden' name='post_id' value='$post_id1'>";
					echo "<input type='hidden' name='description_content' value='$descrip'>";
					echo "<input type='hidden' name='link_content' value='$link'>";
					echo "<input type='hidden' name='photo' value='$photo'>";
					echo "<input type='submit' value='Edit Post'>";
					echo "</form></div>";
					
					echo "<form action='post_delete_commit.php' method='POST'>";
					echo "<input type='hidden' name='post_id' value='$post_id1'>";
					echo "<input type='submit' value='Delete Post'>";
					echo "</form></div>";
					
					}
					echo "</div>";
					
				}
				//random story button
				echo "<a class='w3-btn' href='random_post.php'>A Random Story!</a>";
				
				
				
  
            ?>
  
        </body>
           
        
    </html>


php user: heardditbot; password hearditonthegrapevine

//NEEDS TO BE UPDATED//

//Changed category to not be mandatory//

create table users (
user_id int unsigned auto_increment not null,
username varchar(20) not null unique, 
salt_pass varchar(100),
last_access datetime,
primary key (user_id)
) engine = INNODB DEFAULT character SET = utf8 COLLATE = utf8_general_ci;

create table posts (
post_id int unsigned auto_increment not null,
user_id int unsigned not null,
title tinytext  not null,
description text,
link text  not null,
likes mediumint unsigned  not null,
category enum('Sportsball', 'Diversionary Activities', 'Famous People', 'Smart People', 'Politicians', 'Nerdcore', 'News', 'Cute Animals', 'Not Cute Animals'),
child_id int not null, 
primary key (post_id),
foreign key (user_id) references users (user_id)
) engine = INNODB DEFAULT character SET = utf8 COLLATE = utf8_general_ci;

create table comments (
comment_id mediumint unsigned auto_increment not null,
content text not null,
user_id int unsigned not null,
post_id int unsigned not null,
reply_layer tinyint unsigned,
parent_id mediumint unsigned,
child_id int,
likes mediumint unsigned,
primary key (comment_id),
foreign key (user_id) references users (user_id),
foreign key (post_id) references posts (post_id)
) engine = INNODB DEFAULT character SET = utf8 COLLATE = utf8_general_ci;